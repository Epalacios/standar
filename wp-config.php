<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'standar_web');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', '1234');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '[/~Bz|6r1(k0NtS%0<s(Ghc|&qGC^_<4`]Jp3$ KLJ?r4-|4705[onOPr>AE^%Ik');
define('SECURE_AUTH_KEY', ' I?T$*]u7np)5hwO[`o;$rou2,q*BE<s8&Q:*4]`H:IVP3rb@4Y=~Rg_>Pk,3!/Z');
define('LOGGED_IN_KEY', 'Waq}^/bji2tLD4|#r1d7v oIWdBaH&G9}1&*iN<HaGcNDx^;>~mkGdB,RhJ1yNqK');
define('NONCE_KEY', 'KEs}1mDVt1P#2cWsUsS24y#R7XE_K7+EcZf+f-6|Vb(@~e+$YjI/^T>pjsol)R5Q');
define('AUTH_SALT', 'R20aoz29*@,3DZtu:sec*c@xJ)oE:!Q(OXe;[k$Uqq:HQ.S6v-.koS:TJ-Q)ueNU');
define('SECURE_AUTH_SALT', '3Ng=r,]#W|V)!.:6;wOauEE-Le:Q7-7*<Y&K_-$lhyB)%x6XO+up=O_JV?BDbNgz');
define('LOGGED_IN_SALT', 'UlR*^9$G7/n} ,jld|w!ui<c4E7UJAxfZ`pB{n*Kt9YVqY2.QQ{`d ?WC`N+%!G[');
define('NONCE_SALT', '>arWQXKV:ce#pBNN*DKgkMB3jlpSS%9;yf>kvO`w}+tHEuqv&]X0Ve4$3ESH/2I+');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

define('WP_MEMORY_LIMIT', '512M');

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

